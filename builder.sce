// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

mode(-1);

function main_builder()

  TOOLBOX_NAME  = "agesys";
  TOOLBOX_TITLE = "agesys";
  toolbox_dir   = get_absolute_file_path("builder.sce");

  // Check Scilab's version
  // =============================================================================
  try
	  v = getversion("scilab");
  catch
	  error(gettext("Scilab 5.5 or more is required."));
  end

  if v(2) < 5 then
	  // new API in scilab 5.5
	  error(gettext('Scilab 5.5 or more is required.'));  
  end

  // Check modules_manager module availability
  // =============================================================================
  if ~isdef('tbx_build_loader') then
    error(msprintf(gettext('%s module not installed."), 'modules_manager'));
  end

  // Action
  // =============================================================================
  if ~with_module('xcos') then
    error(msprintf(gettext('%s module not installed."),'xcos'));
  end
  loadXcos();loadXcosLibs();

  // Compile Java file using ant
  if unix("ant -version") <> 0 then
    error("ant could not be found on your system.");
  end

  ECLIPSE_HOME=getenv("ECLIPSE_HOME");
  if ~isdir(ECLIPSE_HOME) then
    error(msprintf(gettext("%s: The directory ""%s"" doesn''t exist.\n"), "builder_java_src", ECLIPSE_HOME));
  end
  if ~isfile(ECLIPSE_HOME+"/eclipse.ini") then
    error(msprintf(gettext("%s: Invalid ""%s"" directory.\n"), "builder_java_src", ECLIPSE_HOME));
  end

  // generate and compile toolbox
  tbx_builder_macros(toolbox_dir);
  tbx_builder_gateway(toolbox_dir);
  tbx_builder_src(toolbox_dir);
  tbx_builder_help(toolbox_dir);

  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);

endfunction
// =============================================================================
main_builder();
clear main_builder;
// =============================================================================
