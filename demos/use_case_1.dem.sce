// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

//SCENARIO POUR LA DEMONSTRATION DU USE CASE 1
//============================================
//
//- loader la toolbox Agesys avec Scilab
//
//- lancer le plugin Papyrus
//
//- ouvrir le schéma "spec_activity_diagram"
//    - Il est composé de 3 diagrammes activités
//        - le diagramme activité principal "Operate Car"
//        - deux diagrammes activités correspondants aux call-behaviours du diagramme principal
//            - le diagramme activité "Enable on Brake Pressure > 0"
//            - le diagramme activité "Monitor Traction"
//
//- Pour activer le use-case 1 :
//    - sur le diagramme principal "Operate Car", faire un click droit sur un des deux call-behaviours
//    - un menu contextuel avec "Launch Scilab / Xcos" apparaît. Cliquer dessus.
//    - Un diagramme Xcos avec le bon nombre d'entrée sortie apparaît
//        - ce sont les Activity Parameter Node qui sont checkés
//        - Sous Papyrus, pour savoir si ce sont des entrée/sortie data/évènement, leur nom est checké. 
//            - inX pour une entrée data
//            - outX pour une sortie data
//            - einX pour une entrée évènement
//            - eoutX pour une entrée évènement
//
//- Voilà, la démo est finie! 
//
//
//
//Points à éclaircir pour notre modélisation du mapping :
//    - les liens data sous Xcos sont modélisés par des flows d'object avec l'attribut "Continuous"
//    - les liens évènement sous Xcos sont modélisé par des flow d'object avec l'attribut "Discrete"
//    - Si un lien data ou évènement est splitté en deux liens, nous utiliserons le Fork node en langage Sysml
//    - Les blocs Xcos sont modélisés par des activités dont les entrées sont modélisés par des Activity Parameter Nodes
//    - La syntaxe utilisé pour les Activity Parameter Nodes est précisé plus haut lors du détail de la démonstration du Use Case 1
//
//

// retrieve current path
path = get_absolute_file_path("use_case_1.dem.sce");

// open this file with scinotes
scinotes(path + filesep() + "use_case_1.dem.sce");

