// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

function loader_java ()
    sci_gateway_dir = get_absolute_file_path("loader.sce");

    // loading xcos addon jar into scilab
    javaclasspath(sci_gateway_dir + "../../../jar/addon.jar");
    system_setproperty("agesys.addon", sci_gateway_dir);
    
    // setting rmi command line and exporting to java runtime
    jre_home = jre_path();
    if (getos() == "Windows") then
        jre_home = getshortpathname(jre_home);
        rmi_registry = jre_home + "\bin\rmiregistry.exe -J-Djava.rmi.server.codebase=file:/" + sci_gateway_dir + "..\..\..\jar\addon.jar";
    else
        rmi_registry = jre_home + "/bin/rmiregistry -J-Djava.rmi.server.codebase=file://" + sci_gateway_dir + "../../../jar/addon.jar";
    end
    system_setproperty("java.rmi.registry", rmi_registry);

endfunction

loader_java();
clear loader_java;

