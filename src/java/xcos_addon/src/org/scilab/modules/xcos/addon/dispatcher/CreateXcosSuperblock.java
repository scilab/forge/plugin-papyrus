// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.dispatcher;

import java.util.ArrayList;
import java.util.Collection;

import org.scilab.modules.types.ScilabDouble;
import org.scilab.modules.types.ScilabString;
import org.scilab.modules.xcos.block.BasicBlock;
import org.scilab.modules.xcos.block.BlockFactory;
import org.scilab.modules.xcos.block.SuperBlock;
import org.scilab.modules.xcos.block.io.ContextUpdate;
import org.scilab.modules.xcos.block.io.ContextUpdate.IOBlocks;
import org.scilab.modules.xcos.graph.SuperBlockDiagram;
import org.scilab.modules.xcos.graph.XcosDiagram;
import org.scilab.modules.xcos.port.command.CommandPort;
import org.scilab.modules.xcos.port.control.ControlPort;
import org.scilab.modules.xcos.port.input.ExplicitInputPort;
import org.scilab.modules.xcos.port.output.ExplicitOutputPort;
import org.scilab.modules.xcos.utils.BlockPositioning;

import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;


public class CreateXcosSuperblock
{
	/**
	 * Constants
	 */
	private static final String INTERFUNCTION_NAME = "SUPER_f";
	
	
	/**
	 * Attributes
	 */

	
	/**
	 * Constructors
	 */
	
	
	/**
	 * Private functions
	 */
	
	/**
	 * Create the corresponding superblock
	 * 
	 * @param diagram : the Xcos diagram container
     * @param superblock_label : the label of the superblock
     * @param numberOfInputs : number of data input of the superblock
     * @param numberOfOutputs : number of data output of the superblock
     * @param numberOfEInputs : number of event input of the superblock
     * @param numberOfEOutputs : number of event output of the superblock
	 */
	private static void processDiagram(XcosDiagram diagram, String superblock_label, int numberOfInputs, int numberOfOutputs,
            						   int numberOfEInputs, int numberOfEOutputs, int id, int numberOfSuperblocks)
	{
		// create the customized Xcos diagram
		createCustomizedDiagram(diagram, superblock_label, numberOfInputs, numberOfOutputs, numberOfEInputs, numberOfEOutputs, id, numberOfSuperblocks);
	}
    
    /**
     * Add the Xcos block label
     * 
     * @param diagram : Xcos diagram container
     * @param block : the block to add the label
     * @param label : the String label
     */
    private static void changeBlockLabel(XcosDiagram diagram, BasicBlock block, String label)
    {
    	final mxCell identifier;
        identifier = diagram.getOrCreateCellIdentifier(block);
        diagram.cellLabelChanged(identifier, label, false);
        diagram.fireEvent(new mxEventObject(mxEvent.LABEL_CHANGED, "cell", identifier, "value", label, "parent", block));
    }
    
    /**
     * Create a customized superblock inside an Xcos diagram
     * 
     * @param diagram : the Xcos diagram container
     * @param superblock_label : the label of the superblock
     * @param numberOfInputs : number of data input of the superblock
     * @param numberOfOutputs : number of data output of the superblock
     * @param numberOfEInputs : number of event input of the superblock
     * @param numberOfEOutputs : number of event output of the superblock
     */
	private static void createCustomizedDiagram (XcosDiagram diagram, String superblock_label, int numberOfInputs, int numberOfOutputs,
			                              int numberOfEInputs, int numberOfEOutputs, int id, int numberOfSuperblocks)
	{
        final SuperBlock superblock = (SuperBlock) BlockFactory.createBlock(INTERFUNCTION_NAME);
        superblock.setStyle(INTERFUNCTION_NAME);

        // Allocate the diagram
        final SuperBlockDiagram diag = new SuperBlockDiagram(superblock);
        superblock.setChild(diag);
        superblock.setParentDiagram(diagram);

        if (numberOfSuperblocks == 1)
        {
        	superblock.getGeometry().setX(250);
        	superblock.getGeometry().setY(150);
        } else
        {
        	superblock.getGeometry().setX((400 - 250) + 250 / ((numberOfSuperblocks - 1)) * id);
        	superblock.getGeometry().setY(150);
        }

        diagram.addCell(superblock);
        
        // set the superblock label
        changeBlockLabel(diagram, superblock, superblock_label);
        
		// add custom ports
		addCellsInSuperDiagram(superblock, numberOfInputs, numberOfOutputs, numberOfEInputs, numberOfEOutputs);

		BlockPositioning.updateBlockView(superblock);
		superblock.updateExportedPort();
        superblock.invalidateRpar();
	}
	
	/**
	 * Add I/O blocks in the superdiagram of a superblock
	 * 
	 * @param superblock : the concerned superblock where to add the I/O blocks
	 * @param numberOfInputs : number of data input port
	 * @param numberOfOutputs : number of data output port
	 * @param numberOfEInputs :  number of event input port
	 * @param numberOfEOutputs : number of event output port
	 */
	private static void addCellsInSuperDiagram (SuperBlock superblock, int numberOfInputs, int numberOfOutputs,
										 int numberOfEInputs, int numberOfEOutputs)
	{
		int index = 0;
		Collection<Object> cellsToCreate = new ArrayList<Object> ();
		
		SuperBlockDiagram superdiagram = superblock.getChild();
		
		// add custom input ports
		for (index = 0; index < numberOfInputs; index ++)
		{
			ExplicitInputPort port = new ExplicitInputPort();
			ContextUpdate ein = IOBlocks.createBlock(port);
			ein.setIntegerParameters(new ScilabDouble(index + 1.0));
			ein.setExprs(new ScilabString(Integer.toString(index + 1)));
			ein.setOrdering(index + 1);
			ein.getGeometry().setX(60);
			ein.getGeometry().setY(60 + index * 80);
			ein.getGeometry().setWidth(20);
			ein.getGeometry().setHeight(20);
			BlockPositioning.updateBlockView(ein);
			changeBlockLabel(superdiagram, ein, "in" + (index + 1));
			cellsToCreate.add(ein);
		}
		// add custom output ports
		for (index = 0; index < numberOfOutputs; index ++)
		{
			ExplicitOutputPort port = new ExplicitOutputPort();
			ContextUpdate eout = IOBlocks.createBlock(port);
			eout.setIntegerParameters(new ScilabDouble(index + 1.0));
			eout.setExprs(new ScilabString(Integer.toString(index + 1)));
			eout.setOrdering(index + 1);
			eout.getGeometry().setX(400);
			eout.getGeometry().setY(60 + index * 80);
			eout.getGeometry().setWidth(20);
			eout.getGeometry().setHeight(20);
			BlockPositioning.updateBlockView(eout);
			changeBlockLabel(superdiagram, eout, "out" + (index + 1));
			cellsToCreate.add(eout);
		}
		// add custom event input ports
		for (index = 0; index < numberOfEInputs; index ++)
		{
			ControlPort port = new ControlPort();
			ContextUpdate evin = IOBlocks.createBlock(port);
			evin.setIntegerParameters(new ScilabDouble(index + 1.0));
			evin.setExprs(new ScilabString(Integer.toString(index + 1)));
			evin.setOrdering(index + 1);
			evin.getGeometry().setX(120 + index * 80);
			evin.getGeometry().setY(30);
			evin.getGeometry().setWidth(20);
			evin.getGeometry().setHeight(20);
			BlockPositioning.updateBlockView(evin);
			changeBlockLabel(superdiagram, evin, "ein" + (index + 1));
			cellsToCreate.add(evin);
		}
		// add custom event output ports
		for (index = 0; index < numberOfEOutputs; index ++)
		{
			CommandPort port = new CommandPort();
			ContextUpdate evout = IOBlocks.createBlock(port);
			evout.setIntegerParameters(new ScilabDouble(index + 1.0));
			evout.setExprs(new ScilabString(Integer.toString(index + 1)));
			evout.setOrdering(index + 1);
			evout.getGeometry().setX(120 + index * 80);
			evout.getGeometry().setY(200);
			evout.getGeometry().setWidth(20);
			evout.getGeometry().setHeight(20);
			BlockPositioning.updateBlockView(evout);
			changeBlockLabel(superdiagram, evout, "eout" + (index + 1));
			cellsToCreate.add(evout);
		}
		superdiagram.addCells(cellsToCreate.toArray());
		superdiagram.setChildrenParentDiagram();
	}
	
	/**
	 * Public functions
	 */
	
	/**
	 * Execute the main task of this class
	 * 
	 * @param parameters : the concerned parameters
	 * @return a null object
	 */
	public static ArrayList<Object> executeTask (ArrayList<Object> parameters)
	{
		XcosDiagram diagram = CreateXcosDiagram.diagram;
		String callBehaviorName = (String) parameters.get(0);
		int numberOfInputs = (Integer) parameters.get(1);
		int numberOfOutputs = (Integer) parameters.get(2);
		int numberOfEInputs = (Integer) parameters.get(3);
		int numberOfEOutputs = (Integer) parameters.get(4);
		int id = (Integer) parameters.get(5);
		int numberOfSuperblocks = (Integer) parameters.get(6);
		
		// launch actions
		processDiagram(diagram, callBehaviorName, numberOfInputs, numberOfOutputs, numberOfEInputs, numberOfEOutputs, id, numberOfSuperblocks);
		
		return null;
	}
}
