// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.visitorpattern;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.scilab.modules.xcos.addon.XcosLibrary;
import org.scilab.modules.xcos.graph.XcosDiagram;


public class ScilabDirectHandler implements Handler 
{
	/**
	 * Constants
	 */
    private static final Logger LOG = Logger.getLogger(ScilabDirectHandler.class.getPackage().getName());
    private static final ScilabDirectHandler INSTANCE = new ScilabDirectHandler();
    private final Semaphore lock = new Semaphore(1, true);

    
    /**
     * Attributes
     */
    
    
    /**
     * Constructors
     */
    public ScilabDirectHandler()
    {

    }

    
    /**
     * Public functions
     */
    
    /*
     * Lock management to avoid multiple actions
     */

    /**
     * Get the current instance of a ScilabDirectHandler.
     *
     * Please note that after calling {@link #acquire()} and performing action,
     * you should release the instance using {@link #release()}.
     *
     * <p>
     * It is recommended practice to <em>always</em> immediately follow a call
     * to {@code getInstance()} with a {@code try} block, most typically in a
     * before/after construction such as:
     *
     * <pre>
     * class X {
     *
     *     // ...
     *
     *     public void m() {
     *         final ScilabDirectHandler handler = ScilabDirectHandler.getInstance();
     *         try {
     *             // ... method body
     *         } finally {
     *             handler.release();
     *         }
     *     }
     * }
     * </pre>
     *
     * @see #release()
     * @return the instance or null if another operation is in progress
     */
    public static ScilabDirectHandler acquire() 
    {
        LOG.finest("lock request");

        try 
        {
            final boolean status = INSTANCE.lock.tryAcquire(0, TimeUnit.SECONDS);
            if (!status) 
            {
                return null;
            }
        } catch (InterruptedException e) 
        {
        	XcosLibrary.LOG.log(Level.SEVERE, XcosLibrary.MSG_EXCEPTION, e);
        }

        LOG.finest("lock acquired");

        return INSTANCE;
    }

    /**
     * Release the instance
     */
    public void release() 
    {
        LOG.finest("lock release");

        INSTANCE.lock.release();
    }

    @Override
    public void writeDiagram(final XcosDiagram diagram) throws Exception 
    {
        LOG.entering("ScilabDirectHandler", "writeDiagram");

        final DiagramElement element = new DiagramElement();
        element.encode(diagram);
    }
}
