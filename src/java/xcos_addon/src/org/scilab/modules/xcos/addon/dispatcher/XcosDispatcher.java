// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.dispatcher;

import java.util.ArrayList;

import org.scilab.modules.xcos.addon.XcosLibrary;


public class XcosDispatcher
{
	/**
	 * Constants
	 */
	public static final String CREATEXCOSDIAGRAMID = "createxcosdiagramid";
	public static final String CREATEXCOSSUPERBLOCKID = "createxcossuperblockid";
	public static final String SAVEXCOSDIAGRAMID = "savexcosdiagramid";
	public static final String CREATEPAPYRUSBLOCK = "createpapyrusblockid";
	public static final String CREATEPAPYRUSLINK = "createpapyruslinkid";
	public static final String CREATEPAPYRUSACTIVITYDIAGRAM = "createpapyrusactivitydiagramid";
	public static final String CREATEPAPYRUSSIMULATIONINFORMATION = "createpapyrussimulationinformationid";
	public static final String DELETEPAPYRUSBLOCK = "deletepapyrusblockid";
	
	
	/**
	 * Attributes
	 */
	
	
    /**
     * 
     */
    public XcosDispatcher ()
    {

    }
    
	/**
	 * Private functions
	 */
	
	
	/**
	 * Public functions
	 */
    
    /**
     * Dispatch action in Xcos side depending on the id message
     * 
     * @param id : id of the message
     * @param parameters : parameters to deal with
     * @return out parameters
     */
	public static ArrayList<Object> dispatch(String id, ArrayList<Object> parameters)
	{
		ArrayList<Object> result = null;
		
		if (id.equals(CREATEXCOSDIAGRAMID))
		{
			result = CreateXcosDiagram.executeTask(parameters);
		} else if (id.equals(CREATEXCOSSUPERBLOCKID))
		{
			result = CreateXcosSuperblock.executeTask(parameters);
		} else if (id.equals(SAVEXCOSDIAGRAMID))
		{
			result = SaveXcosDiagram.executeTask(parameters);
		}
		else
		{
			XcosLibrary.LOG.severe("\tUnknown dispatcher identification");
		}
		
		return result;
	}
}
