// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.dispatcher;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.scilab.modules.xcos.Xcos;
import org.scilab.modules.xcos.graph.XcosDiagram;


public class SaveXcosDiagram
{
	/**
	 * Constants
	 */
	
	
	/**
	 * Attributes
	 */
	private static String diagram_path;
	
	
	/**
	 * Constructors
	 */
	
	
	/**
	 * Private functions
	 */
	
	/**
	 * Create an Xcos diagram with the corresponding superblock
	 */
	private static void processDiagram(XcosDiagram diagram, String workspaceName, String callBehaviorName)
	{
		// save the diagram inside namespace
		saveXcosDiagram(diagram, workspaceName, callBehaviorName);
		
		// launch the diagram with Xcos
		Xcos.xcos(diagram_path, null);
	}
	
	/**
	 * Save the Xcos diagram in a file
	 * 
	 * @param diagram : the Xcos diagram
	 */
	private static void saveXcosDiagram (XcosDiagram diagram, String workspaceName, String callBehaviorName)
	{
		diagram_path = workspaceName + "/sysml2xcos_" + callBehaviorName + ".zcos";
		File diagram_file = new File(diagram_path);
		if (diagram_file.exists())
		{
			String [] choice = new String [2];
			choice[0] = "Delete existing Xcos diagram";
			choice[1] = "Open existing Xcos diagram";
			int rang = JOptionPane.showOptionDialog(null, "Choose among the following actions:",
				      					 "Xcos diagram detected...", JOptionPane.YES_NO_OPTION,
				      					 JOptionPane.QUESTION_MESSAGE, null, choice, choice[1]);
			if (rang == 0)
			{
				diagram_file.delete();
				diagram.getRootDiagram().saveDiagramAs(diagram_file);
			} else
			{
				diagram.transformAndLoadFile(diagram_file.getAbsolutePath(), null);
			}
		} else
		{
			diagram.getRootDiagram().saveDiagramAs(diagram_file);
		}
	}
	
	/**
	 * Public functions
	 */
	
	/**
	 * Execute the main task of this class
	 * 
	 * @param parameters : the concerned parameters
	 * @return a null object
	 */
	public static ArrayList<Object> executeTask (ArrayList<Object> parameters)
	{
		XcosDiagram diagram = CreateXcosDiagram.diagram;
		String workspaceName = (String) parameters.get(0);
		String callBehaviorName = (String) parameters.get(1);
		
		// launch actions
		processDiagram(diagram, workspaceName, callBehaviorName);
		
		return null;
	}
}
