// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.scilab.modules.xcos.addon.visitorpattern;

import org.scilab.modules.xcos.block.BasicBlock;

/**
 * Abstract class which provide commons method for any class that works with
 * block element.
 * 
 * This class is intentionally package protected.
 */
abstract class BlockPartsElement extends AbstractElement<BasicBlock> 
{
    /** input port size */
    private int inSize;
    /** output port size */
    private int outSize;
    /** event input (control) port size */
    private int einSize;
    /** event output (command) port size */
    private int eoutSize;

    /**
     * Default constructor
     */
    protected BlockPartsElement() 
    {
    	
    }

    /**
     * Set the current port size.
     * 
     * @param in
     *            the input port vector size
     * @param out
     *            the output port vector size
     * @param ein
     *            the event input (control) port vector size
     * @param eout
     *            the event output (command) port vector size
     */
    protected void setPortsSize(int in, int out, int ein, int eout) 
    {
        this.inSize = in;
        this.outSize = out;
        this.einSize = ein;
        this.eoutSize = eout;
    }

    /**
     * @return the input port vector size
     */
    protected final int getInSize() 
    {
        return inSize;
    }

    /**
     * @return the output port vector size
     */
    protected final int getOutSize() 
    {
        return outSize;
    }

    /**
     * @return the event input (control) port vector size
     */
    protected final int getEinSize() 
    {
        return einSize;
    }

    /**
     * @return the event output (command) port vector size
     */
    protected final int getEoutSize() 
    {
        return eoutSize;
    }
}
