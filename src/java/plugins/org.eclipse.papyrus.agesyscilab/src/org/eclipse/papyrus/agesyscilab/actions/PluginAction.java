// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.eclipse.papyrus.agesyscilab.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.Element;
import org.eclipse.emf.facet.infra.browser.uicore.internal.model.ModelElementItem;

import org.scilab.modules.xcos.addon.rmi.ComputeInEclipse;
import org.scilab.modules.xcos.addon.dispatcher.XcosDispatcher;
import org.eclipse.papyrus.agesyscilab.actions.distpatcher.*;

import static org.eclipse.papyrus.agesyscilab.Activator.log;


/**
 *
 */
@SuppressWarnings("restriction")
public class PluginAction extends AbstractHandler implements IHandler, ComputeInEclipse
{
	/*
	 * Constants
	 */
	
	
	/*
	 * Attributes
	 */
	private Activity calledActivity;
	private String callBehaviorName;
	
	
	/*
	 * Constructors
	 */
	public PluginAction() 
	{

	}

	
	/*
	 * Privates functions
	 */
	
	/**
	 * Reset class attributes to their default value
	 */
	private void resetAttributes()
	{
		this.callBehaviorName = null;
	}
	
	/**
	 * Get a list of selected element in the current active diagram
	 * 
	 * @return this list of element
	 * @throws Exception
	 */
	private List<?> lookupSelectedElements() throws Exception
	{
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		ISelection selection = page.getSelection();
		
		if(selection instanceof IStructuredSelection) 
		{
			IStructuredSelection structuredSelection = (IStructuredSelection)selection;
			return structuredSelection.toList();
		} 
		else if (selection instanceof TreeSelection) 
		{
			TreeSelection treeSelection = (TreeSelection)selection;
			return treeSelection.toList();
		}
		
		return null;
	}
	
	/**
	 * Convertion function
	 * 
	 * @return a list of IAdaptable element
	 * @throws Exception
	 */
	private List<IAdaptable> getSelectedIAdaptableObject() throws Exception
	{

		List<?> selections = lookupSelectedElements();
		List<IAdaptable> results = new ArrayList<IAdaptable>();

		for( Object obj : selections) 
		{
			if( obj instanceof IAdaptable ) 
			{
				IAdaptable adaptable = (IAdaptable) obj; 
				if (adaptable != null) 
				{
					results.add(adaptable);
				}
			}
		}
		
		return results;
	}
	
	/**
	 * Get the first EMF selected object
	 * 
	 * @return this EMF selected object
	 * @throws Exception
	 */
	private ArrayList<Element> getSysmlSelectedElements() throws Exception
	{
		ArrayList<Element> elementList = new ArrayList<Element> ();
		
		List<IAdaptable> listNamedElement = getSelectedIAdaptableObject();

		for (IAdaptable adaptable : listNamedElement)
		{
			Element selectedElement = (Element) adaptable.getAdapter(Element.class);
			if (selectedElement == null)
			{
				if (adaptable instanceof ModelElementItem)
				{
					ModelElementItem mei = (ModelElementItem) adaptable;
					selectedElement = (Element) mei.getEObject();
				}
			}
			if (selectedElement != null)
			{
				elementList.add(selectedElement);
			}
		}

		return elementList;
	}
	
	/**
	 * Analyze a call behavior action EMF object to get its number of ports
	 * 
	 * @param element : the call behavior action EMF object to analyse
	 * @return a list of integer representing the different number of ports
	 * @throws Exception
	 */
	private ArrayList<Object> analyseCallBehaviour (CallBehaviorAction element) throws Exception
	{
		ArrayList<Object> characteristicsList = new ArrayList<Object> ();
		
		Behavior calledBehavior = element.getBehavior();
		callBehaviorName = calledBehavior.getName();
		if (calledBehavior instanceof Activity)
		{
			int numberOfInputs = 0;
			int numberOfOutputs = 0;
			int numberOfEInputs = 0;
			int numberOfEOutputs = 0;
			int index = 0;
			calledActivity = (Activity) calledBehavior;
			
			List<ActivityNode> nodesList = calledActivity.getNodes();
			for (index = 0; index < nodesList.size(); index++)
			{
				ActivityNode actNode = nodesList.get(index);
				if (actNode instanceof ActivityParameterNode)
				{
					String nodename = actNode.getName();
					int indexin = nodename.indexOf("in");
					int indexout = nodename.indexOf("out");
					int indexein = nodename.indexOf("ein");
					int indexeout = nodename.indexOf("eout");
					if (indexin == 0)
					{
						numberOfInputs++;
					}
					if (indexout == 0)
					{
						numberOfOutputs++;
					}
					if (indexein == 0)
					{
						numberOfEInputs++;
					}
					if (indexeout == 0)
					{
						numberOfEOutputs++;
					}
				}
			}
			characteristicsList.add(numberOfInputs);
			characteristicsList.add(numberOfOutputs);
			characteristicsList.add(numberOfEInputs);
			characteristicsList.add(numberOfEOutputs);
		}
		
		return characteristicsList;
	}
	
	/**
	 * Analyze an activity EMF object to get its number of ports
	 * 
	 * @param element : the Activity EMF object to analyze
	 * @return a list of integers representing the different number of ports
	 * @throws Exception
	 */
	private ArrayList<Object> analyseActivity (Activity element) throws Exception
	{
		ArrayList<Object> characteristicsList = new ArrayList<Object> ();

		int numberOfInputs = 0;
		int numberOfOutputs = 0;
		int numberOfEInputs = 0;
		int numberOfEOutputs = 0;
		int index = 0;
		calledActivity = element;

		List<ActivityNode> nodesList = calledActivity.getNodes();
		for (index = 0; index < nodesList.size(); index++)
		{
			ActivityNode actNode = nodesList.get(index);
			if (actNode instanceof ActivityParameterNode)
			{
				String nodename = actNode.getName();
				int indexin = nodename.indexOf("in");
				int indexout = nodename.indexOf("out");
				int indexein = nodename.indexOf("ein");
				int indexeout = nodename.indexOf("eout");
				if (indexin == 0)
				{
					numberOfInputs++;
				}
				if (indexout == 0)
				{
					numberOfOutputs++;
				}
				if (indexein == 0)
				{
					numberOfEInputs++;
				}
				if (indexeout == 0)
				{
					numberOfEOutputs++;
				}
			}
		}
		characteristicsList.add(numberOfInputs);
		characteristicsList.add(numberOfOutputs);
		characteristicsList.add(numberOfEInputs);
		characteristicsList.add(numberOfEOutputs);

		return characteristicsList;
	}
	
	/**
	 * Analyze an EMF element to determine its number of ports
	 * 
	 * @param element : the EMF object to analyse
	 * @return the list of integer representing the different number of ports
	 * @throws Exception
	 */
	private ArrayList<Object> analyseSysmlElement(Element element) throws Exception
	{
		ArrayList<Object> characteristicsList = null;
		
		if (element instanceof CallBehaviorAction)
		{
			CallBehaviorAction callbehavioraction = (CallBehaviorAction) element;
			characteristicsList = analyseCallBehaviour(callbehavioraction);
		} else if (element instanceof Activity)
		{
			Activity activity = (Activity) element;
			characteristicsList = analyseActivity(activity);
		}
		
		return characteristicsList;
	}
	
	/**
	 * Get the current workspace name
	 * 
	 * @return the current workspace name
	 * @throws Exception
	 */
	private String getWorkspaceName () throws Exception
	{
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IPath location = root.getLocation();
		String current_workspace = location.toPortableString();
		
		return current_workspace;
	}
	
	/**
	 * Map into Xcos the corresponding call behavior action EMF object
	 * 
	 * @param characteristicsList : the list of characteristics of the EMF object to map
	 * @param id : its id in the user selection
	 * @param numberOfSelectedBlocks : the number of selected blocks in Papyrus
	 * @throws Exception 
	 */
	private void sendScilabCallBehaviourJob (ArrayList<Object> characteristicsList, int id, int numberOfSelectedBlocks) throws Exception
	{
		int numberOfInputs = (Integer) characteristicsList.get(0);
		int numberOfOutputs = (Integer) characteristicsList.get(1);
		int numberOfEInputs = (Integer) characteristicsList.get(2);
		int numberOfEOutputs = (Integer) characteristicsList.get(3);

		ArrayList<Object> parameters = new ArrayList<Object> ();
		parameters.add(callBehaviorName);
		parameters.add(numberOfInputs);
		parameters.add(numberOfOutputs);
		parameters.add(numberOfEInputs);
		parameters.add(numberOfEOutputs);
		parameters.add(id);
		parameters.add(numberOfSelectedBlocks);
		
		// send job to Scilab / Xcos through java rmi mecanism
		ConnectionManager.comp.computeFromEclipseToXcos(XcosDispatcher.CREATEXCOSSUPERBLOCKID, parameters);
	}
	
	/**
	 * Create an empty Xcos diagram
	 * 
	 * @throws Exception
	 */
	private void createEmptyXcosDiagram() throws Exception
	{
		// send job to Scilab / Xcos through java rmi mecanism
		ConnectionManager.comp.computeFromEclipseToXcos(XcosDispatcher.CREATEXCOSDIAGRAMID, null);
	}
	
	/**
	 * Save the Xcos diagram previously created and open it
	 */
	private void SaveOpenXcosDiagram() throws Exception
	{
		String workspaceName = getWorkspaceName();
		
		ArrayList<Object> parameters = new ArrayList<Object> ();
		parameters.add(workspaceName);
		parameters.add(callBehaviorName);
		
		// send job to Scilab / Xcos through java rmi mecanism
		ConnectionManager.comp.computeFromEclipseToXcos(XcosDispatcher.SAVEXCOSDIAGRAMID, parameters);
	}
	
	/**
	 * Process the different action to map a call behavior action EMF object 
	 * or an activity EMF object into Xcos.
	 * 
	 * @throws Exception
	 */
	private void process () throws Exception
	{
		// create an empty Xcos diagram
		createEmptyXcosDiagram();
		
		// get the element to map from Papyrus to Xcos
		ArrayList<Element> elementList = getSysmlSelectedElements();
		
		// map each Papyrus element into an Xcos superblock
		for (int i = 0; i < elementList.size(); i++)
		{
			Element element = elementList.get(i);
			
			log.info("\t- Analysing Papyrus selected element");
			ArrayList<Object> characteristicsList = analyseSysmlElement(element);
		
			// map it under Scilab / Xcos
			if (characteristicsList != null)
			{
				sendScilabCallBehaviourJob(characteristicsList, i, elementList.size());
			} else
			{
				log.info("\t- Unable to map the equivalent Xcos superblock");
			}
		}
		
		// save the result diagram into a file and open it
		SaveOpenXcosDiagram();
	}
	
	/*
	 * Public functions
	 */

	/**
	 * Compute an action method ordered by Xcos inside Eclipse
	 */
	@Override
	public ArrayList<Object> computeFromXcosToEclipse(String id, ArrayList<Object> parameters)
	{
		ArrayList<Object> result = null;
		
		try 
		{
			result = PapyrusDispatcher.dispatch(id, parameters);
		} catch (Exception e)
		{
			log.error(e);
			ConnectionManager.rmiRegistrationDone = false;
		}
		
		return result;
	}

	/**
	 * On a user defined action, execute this function. 
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		log.info("\n--> Papyrus Plugin");
		
		try 
		{
			// reset privates attributes
			resetAttributes();

			process();
			
			PapyrusLibrary.dialogInformation("Xcos superblock(s) creation succeeded.");
		} catch (Exception e)
		{
			log.error(e);
			PapyrusLibrary.dialogError("Unable to map Papyrus block");
			ConnectionManager.rmiRegistrationDone = false;
		}

		log.info("<-- Papyrus Plugin\n");

		return null;
	}
}
