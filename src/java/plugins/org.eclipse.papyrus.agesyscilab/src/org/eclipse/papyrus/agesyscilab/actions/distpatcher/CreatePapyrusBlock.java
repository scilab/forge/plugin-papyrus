// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.eclipse.papyrus.agesyscilab.actions.distpatcher;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.infra.core.utils.ServiceUtils;
import org.eclipse.papyrus.uml.diagram.activity.edit.parts.InputPinInCBActLabelEditPart;
import org.eclipse.papyrus.uml.diagram.activity.edit.parts.InputPinInCallBeActEditPart;
import org.eclipse.papyrus.uml.diagram.activity.edit.parts.OutputPinInCallBeActEditPart;
import org.eclipse.papyrus.uml.diagram.activity.edit.parts.OutputPinInCBActLabelEditPart;
import org.eclipse.papyrus.uml.diagram.common.editparts.NamedElementEditPart;
import org.eclipse.papyrus.uml.diagram.common.locator.PortPositionLocator;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import static org.eclipse.papyrus.agesyscilab.Activator.log;

import org.eclipse.papyrus.agesyscilab.actions.*;


public class CreatePapyrusBlock
{
	/**
	 * Constants
	 */
	public static final String UID_EANNOTATION = "uid";
	public static final double SCALE_FACTOR = 1.4;
	
	
	/**
	 * Attributes
	 */
	
	
	/**
	 * Constructors
	 */
	
	
	/**
	 * Private functions
	 */
	
	/**
	 * Store a String object inside an EMF object.
	 * EAnnotation are used.
	 * 
	 * @param element : the containing EMF object
	 * @param value : the String information
	 */
	private static void createParameters(final NamedElement element, final String value)
	{
		final Comment c = (Comment) PapyrusLibrary.createSysmlElementFromUmlType(element, UMLElementTypes.COMMENT, null);
		
		try 
		{	
			TransactionalEditingDomain editingDomain = ServiceUtils.getInstance().getTransactionalEditingDomain(PapyrusLibrary.getServiceRegistry());
			CommandStack stack = editingDomain.getCommandStack();
			RecordingCommand rc = new RecordingCommand(editingDomain)
			{
				@Override
				protected void doExecute() {
					c.setBody(value);
					EAnnotation ea = element.createEAnnotation(UID_EANNOTATION);
					ea.getContents().add(c);
					element.getEAnnotations().add(ea);
				}
			};
			stack.execute(rc);
		} catch (Exception e)
		{
			log.error(e);
		}
	}
	
	/**
	 * Set the direction of an EMF parameter inside an activity parameter node
	 * 
	 * @param activityParameterNode : The containing activity parameter node
	 * @param p : the EMF parameter object to apply to the activity parameter node
	 * @param direction : the direction of the EMF parameter object to set
	 * @param name : the name of the EMF parameter object to set
	 */
	private static void modifyParameterFromActivityParameterNode(final ActivityParameterNode activityParameterNode, final Parameter p, final ParameterDirectionKind direction, final String name)
	{
		try
		{
			TransactionalEditingDomain editingDomain = ServiceUtils.getInstance().getTransactionalEditingDomain(PapyrusLibrary.getServiceRegistry());
			CommandStack stack = editingDomain.getCommandStack();
			RecordingCommand rc = new RecordingCommand(editingDomain)
			{

				@Override
				protected void doExecute() {
					p.setDirection(direction);
					p.setName(name);
					activityParameterNode.setParameter(p);
				}
			};
			stack.execute(rc);
		} catch (Exception e)
		{
			log.error(e);
		}
	}
	
	/**
	 * Create all the necessary activity parameter nodes inside an activity given the number of inputs and outputs
	 * 
	 * @param activity : the containing activity
	 * @param nb_in_port : number of data input ports
	 * @param nb_out_port : number of data output ports
	 * @param nb_ein_port : number of event input ports
	 * @param nb_eout_port : number of event output ports
	 * @param uid : the String uid to set to the activity
	 */
	private static void createAndSetActivityparameters(final Activity activity, int nb_in_port, int nb_out_port, int nb_ein_port, int nb_eout_port,
												String uid)
	{
		for (int i = 0; i < nb_in_port; i++)
		{
			String name = "in" + (i + 1);
			Parameter pin = (Parameter) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.PARAMETER, null);
			ActivityParameterNode activityParameterNode = (ActivityParameterNode) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.ACTIVITY_PARAMETER_NODE, pin);
			PapyrusLibrary.modifyNameFromNamedElement(activityParameterNode, name);
			modifyParameterFromActivityParameterNode(activityParameterNode, pin, ParameterDirectionKind.IN_LITERAL, name);
		}
		
		for (int i = 0; i < nb_out_port; i++)
		{
			String name = "out" + (i + 1);
			Parameter pin = (Parameter) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.PARAMETER, null);
			ActivityParameterNode activityParameterNode = (ActivityParameterNode) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.ACTIVITY_PARAMETER_NODE, pin);
			PapyrusLibrary.modifyNameFromNamedElement(activityParameterNode, name);
			modifyParameterFromActivityParameterNode(activityParameterNode, pin, ParameterDirectionKind.OUT_LITERAL, name);
		}
		
		for (int i = 0; i < nb_ein_port; i++)
		{
			String name = "ein" + (i + 1);
			Parameter pin = (Parameter) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.PARAMETER, null);
			ActivityParameterNode activityParameterNode = (ActivityParameterNode) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.ACTIVITY_PARAMETER_NODE, pin);
			PapyrusLibrary.modifyNameFromNamedElement(activityParameterNode, name);
			modifyParameterFromActivityParameterNode(activityParameterNode, pin, ParameterDirectionKind.IN_LITERAL, name);
		}
		
		for (int i = 0; i < nb_eout_port; i++)
		{
			String name = "eout" + (i + 1);
			Parameter pin = (Parameter) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.PARAMETER, null);
			ActivityParameterNode activityParameterNode = (ActivityParameterNode) PapyrusLibrary.createSysmlElementFromUmlType(activity, UMLElementTypes.ACTIVITY_PARAMETER_NODE, pin);
			PapyrusLibrary.modifyNameFromNamedElement(activityParameterNode, name);
			modifyParameterFromActivityParameterNode(activityParameterNode, pin, ParameterDirectionKind.OUT_LITERAL, name);
		}
		
		// set block uid in eannotation of the activity
		createParameters(activity, uid);
	}
	
	/**
	 * Create a SysML block at the given coordinates with the right number of ports
	 * 
	 * @param name : the name of the SysML block to create
	 * @param x : the x coordinate of the SysML object to create
	 * @param y : the y coordinate of the SysML object to create
	 * @param nb_in_port : the number of data input ports
	 * @param nb_out_port : the number of data output ports
	 * @param nb_ein_port : the number of event input ports
	 * @param nb_eout_port : the number of event output ports
	 * @param uid : the string uid to set to the SysML object
	 * @param diagram_uid : the String uid of the EMF container
	 * @param rpar : the Xcos rpar value of the object to create
	 */
	private static void createSysmlItem(String name, int x, int y,
								 int nb_in_port, int nb_out_port, int nb_ein_port, int nb_eout_port,
								 String uid, String diagram_uid, String rpar)
	{
		log.info("--> CreatePapyrusBlock");
		
		if (name.equals("SPLIT_f") ||
			name.equals("CLKSPLIT_f"))
		{
			// getting the name container of this block
			NamedElement container = PapyrusLibrary.getElementByUid(null, diagram_uid);
			
			// Create the forknode model object
			ForkNode forknode = (ForkNode) PapyrusLibrary.createSysmlElementFromUmlType(container, UMLElementTypes.FORK_NODE, null);
			PapyrusLibrary.modifyNameFromNamedElement(forknode, name);
			
			// set block uid in eannotation of the activity
			createParameters(forknode, uid);
			
			// get the unique diagram contained in this container (same name as the container)
			Diagram diagram = null;
			Object [] diagram_array = PapyrusLibrary.evaluate(container).toArray();
			if (diagram_array.length == 1)
			{
				diagram = (Diagram) diagram_array[0];
			}
			
			PapyrusLibrary.openDiagram(diagram);
			IEditorPart activeEditor = PapyrusLibrary.getISashWindowsContainer().getActiveEditor();
			EditPart editpart = (EditPart) activeEditor.getAdapter(EditPart.class);
			
			// droping an element to the selectect diagram
			PapyrusLibrary.showElementIn(forknode, (DiagramEditor) activeEditor, editpart, x, y);
		} else if ((name.equals("IN_f")) ||
				   (name.equals("OUT_f")) ||
				   (name.equals("CLKINV_f")) ||
				   (name.equals("CLKOUTV_f")))
		{
			// getting the name container of this block
			Activity container = (Activity) PapyrusLibrary.getElementByUid(null, diagram_uid);
			
			// get container uid
			List<Element> list_nodes =  container.getOwnedElements();
			ActivityParameterNode apn = null;
			double rpar_int = -1;
			try {
				rpar_int = Double.parseDouble(rpar);
			} catch (Exception e)
			{
				//log.error(e);
			}
			for (Element node : list_nodes)
			{
				if (node instanceof ActivityParameterNode)
				{
					ActivityParameterNode activity_parameter_node = (ActivityParameterNode) node;
					String node_name = activity_parameter_node.getName(); // for instance in1 or eout2
					String str_temp = node_name.replaceAll("in", "");
					str_temp = str_temp.replaceAll("out", "");
					str_temp = str_temp.replaceAll("e", "");
					double node_number = 0;
					if (str_temp.length() > 0)
					{
						node_number = Double.parseDouble(str_temp);
					}

					
					if (((name.equals("IN_f")) && (node_name.indexOf("in") > -1) && (rpar_int == node_number)) ||
						((name.equals("CLKINV_f")) && (node_name.indexOf("ein") > -1) && (rpar_int == node_number)) ||
						((name.equals("OUT_f")) && (node_name.indexOf("out") > -1) && (rpar_int == node_number)) ||
						((name.equals("CLKOUTV_f")) && (node_name.indexOf("eout") > -1)) && (rpar_int == node_number))
					{
							apn = activity_parameter_node;
					}
					
				}
			}
			
			// set block uid in eannotation of the activity
			createParameters(apn, uid);
		} else 
		{
			// getting the name container of this block
			NamedElement container = PapyrusLibrary.getElementByUid(null, diagram_uid);
			
			// Create the activity model object
			Activity activity = (Activity) PapyrusLibrary.createSysmlElementFromUmlType(container, UMLElementTypes.ACTIVITY, null);
			PapyrusLibrary.modifyNameFromNamedElement(activity, name);

			// create the activity parameters of the activity
			createAndSetActivityparameters(activity, nb_in_port, nb_out_port, nb_ein_port, nb_eout_port, uid);

			// Create the call-behavior model object
			CallBehaviorAction callbehavior = (CallBehaviorAction) PapyrusLibrary.createSysmlElementFromUmlType(container, UMLElementTypes.CALL_BEHAVIOR_ACTION, activity);
			PapyrusLibrary.modifyNameFromNamedElement(callbehavior, name);
						
			// get the unique diagram contained in this container (same name as the container)
			Diagram diagram = null;
			Object [] diagram_array = PapyrusLibrary.evaluate(container).toArray();
			if (diagram_array.length == 1)
			{
				diagram = (Diagram) diagram_array[0];
			}
			
			PapyrusLibrary.openDiagram(diagram);
			IEditorPart activeEditor = PapyrusLibrary.getISashWindowsContainer().getActiveEditor();
			EditPart editpart = (EditPart) activeEditor.getAdapter(EditPart.class);
			
			// pushing an SysML object to the current diagram graphically
			EditPart result_part = PapyrusLibrary.showElementIn(callbehavior, (DiagramEditor) activeEditor, editpart, x, y);
			
			// ordering the pins of the named element
			if (result_part instanceof NamedElementEditPart)
			{
				NamedElementEditPart neep = (NamedElementEditPart) result_part;
				Point parentLoc = neep.getLocation().getCopy();
				
				List<?> neep_children = neep.getChildren();
				for (int i = 0; i < neep_children.size(); i++)
				{
					if (neep_children.get(i) instanceof InputPinInCallBeActEditPart)
					{
						InputPinInCallBeActEditPart inputpin = (InputPinInCallBeActEditPart) neep_children.get(i);
						
						ChangeBoundsRequest req = new ChangeBoundsRequest(RequestConstants.REQ_MOVE);
						req.setEditParts(inputpin);

						List<?> inputpin_children = inputpin.getChildren();
						String pin_label = "";
						for (int j = 0; j < inputpin_children.size(); j++)
						{
							if (inputpin_children.get(j) instanceof InputPinInCBActLabelEditPart)
							{
								InputPinInCBActLabelEditPart inputpin_label = (InputPinInCBActLabelEditPart) inputpin_children.get(j);
								pin_label = inputpin_label.getEditText();
								break;
							}
						}
						PortPositionLocator locator = new PortPositionLocator(neep.getFigure(), PositionConstants.NONE);
						Point dropLocation = parentLoc;
						if (pin_label.indexOf("e") > -1)
						{
							dropLocation = new Point(0, 500);
						} else
						{
							dropLocation = new Point(200, 0);
						}
			            Rectangle proposedBounds = new Rectangle(dropLocation, new Dimension(20, 20));
			            proposedBounds = proposedBounds.getTranslated(parentLoc);
			            Rectangle preferredBounds = locator.getPreferredLocation(proposedBounds);
			            Rectangle creationBounds = preferredBounds.getTranslated(parentLoc.getNegated());
			            dropLocation = creationBounds.getLocation();
			            
			            req.setLocation(dropLocation);
			            org.eclipse.gef.commands.Command commandChangeBoundsRequest = inputpin.getCommand(req);
			            //((DiagramEditor) activeEditor).getDiagramEditDomain().getDiagramCommandStack().execute(commandChangeBoundsRequest);
					}
					
					if (neep_children.get(i) instanceof OutputPinInCallBeActEditPart)
					{
						OutputPinInCallBeActEditPart outputpin = (OutputPinInCallBeActEditPart) neep_children.get(i);
					
						ChangeBoundsRequest req = new ChangeBoundsRequest(RequestConstants.REQ_MOVE);
						req.setEditParts(outputpin);
					
						List<?> outputpin_children = outputpin.getChildren();
						String pin_label = "";
						for (int j = 0; j < outputpin_children.size(); j++)
						{
							if (outputpin_children.get(j) instanceof OutputPinInCBActLabelEditPart)
							{
								OutputPinInCBActLabelEditPart outputpin_label = (OutputPinInCBActLabelEditPart) outputpin_children.get(j);
								pin_label = outputpin_label.getEditText();
								break;
							}
						}
						PortPositionLocator locator = new PortPositionLocator(neep.getFigure(), PositionConstants.NONE);
						Point dropLocation = parentLoc;
						if (pin_label.indexOf("e") > -1)
						{
							dropLocation = new Point(500, 0);
						} else
						{
							dropLocation = new Point(0, 1000);
						}
			            Rectangle proposedBounds = new Rectangle(dropLocation, new Dimension(20, 20));
			            proposedBounds = proposedBounds.getTranslated(parentLoc);
			            Rectangle preferredBounds = locator.getPreferredLocation(proposedBounds);
			            Rectangle creationBounds = preferredBounds.getTranslated(parentLoc.getNegated());
			            dropLocation = creationBounds.getLocation();
			            
			            req.setLocation(dropLocation);
			            org.eclipse.gef.commands.Command commandChangeBoundsRequest = outputpin.getCommand(req);
			            //((DiagramEditor) activeEditor).getDiagramEditDomain().getDiagramCommandStack().execute(commandChangeBoundsRequest);
					}
				}
			}
		}
		log.info("<-- CreatePapyrusBlock\n");
	}
	
	/**
	 * Public functions
	 */
	
	/**
	 * Execute the main action of this class
	 * 
	 * @param parameters : all the parameters needed
	 * @return a null object
	 */
	public static ArrayList<Object> executeTask(final ArrayList<Object> parameters)
	{		
		Display.getDefault().syncExec(
				new Runnable() 
				{
					public void run()
					{ 
						final String name = (String) parameters.get(0);
						final int x = (int) (((Integer) parameters.get(1)) * SCALE_FACTOR);
						final int y = (int) (((Integer) parameters.get(2)) * SCALE_FACTOR);
						final int nb_in_port = (Integer) parameters.get(3);
						final int nb_out_port = (Integer) parameters.get(4);
						final int nb_ein_port = (Integer) parameters.get(5);
						final int nb_eout_port = (Integer) parameters.get(6);
						final String uid = (String) parameters.get(7);
						final String diagram_uid = (String) parameters.get(8);
						final String rpar = (String) parameters.get(9);
						
						createSysmlItem(name, x, y, nb_in_port, nb_out_port, nb_ein_port, nb_eout_port, uid, diagram_uid, rpar);
					} 
				});
		
		return null;
	}
}
