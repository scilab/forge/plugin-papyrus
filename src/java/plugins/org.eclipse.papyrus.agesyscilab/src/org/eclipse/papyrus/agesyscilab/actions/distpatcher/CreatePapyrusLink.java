// Copyright Scilab Enterprises - 2013
// See http://www.scilab-enterprises.com
//
// This software is a computer program whose purpose is to deal with the
// Agesys project. 
// See http://www.systematic-paris-region.org/en/projets/agesys
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

package org.eclipse.papyrus.agesyscilab.actions.distpatcher;

import java.util.ArrayList;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.infra.core.utils.ServiceUtils;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.CallBehaviorAction;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.Stereotype;

import org.eclipse.papyrus.agesyscilab.actions.*;

import static org.eclipse.papyrus.agesyscilab.Activator.log;


public class CreatePapyrusLink
{
	/**
	 * Constants
	 */
	
	
	/**
	 * Attributes
	 */
	
	
	/**
	 * Constructors
	 */
	
	
	/**
	 * Private functions
	 */
	
	/**
	 * Add a Stereotype EMF object to an EMF object flow object
	 * 
	 * @param objectflow : the containing object flow object
	 * @param stereotype : the stereotype to deal with the object flow
	 */
	private void addStereotypeFromObjectFlow(final ObjectFlow objectflow, final String stereotype)
	{
		final Stereotype s = (Stereotype) PapyrusLibrary.createSysmlElementFromUmlType(objectflow, UMLElementTypes.STEREOTYPE, null, null);
		
		try {
			TransactionalEditingDomain editingDomain = ServiceUtils.getInstance().getTransactionalEditingDomain(PapyrusLibrary.getServiceRegistry());
			CommandStack stack = editingDomain.getCommandStack();
			RecordingCommand rc = new RecordingCommand(editingDomain)
			{
				@Override
				protected void doExecute() {
					s.setName(stereotype);
					objectflow.getAppliedStereotypes().add(s);
				}
			};
			stack.execute(rc);

		} catch (Exception e)
		{
			log.error(e);
		}
	}
	
	/**
	 * Create an EMF object flow from a source EMF object to a target EMF object
	 * 
	 * @param name : the name of the object flow
	 * @param block_source : the String uid of the block source
	 * @param port_src_name : the name of the source port
	 * @param block_target : the String uid of the block target
	 * @param port_dst_name : the name of the target port
	 * @param diagram_uid : the String uid of the container of the object flow
	 */
	private static void createSysmlItem(String name, String block_source, String port_src_name,
								 String block_target, String port_dst_name, String diagram_uid)
	{
		log.info("--> CreatePapyrusLink");
		
		// getting the name container of this block
		NamedElement container = PapyrusLibrary.getElementByUid(null, diagram_uid);
		
		// get source and destination NamedElement
		EList<Element> listOfElement = container.allOwnedElements();
		CallBehaviorAction callbehaviour_src = null;
		CallBehaviorAction callbehaviour_dst = null;
		ForkNode forknode_src = null;
		ForkNode forknode_dst = null;
		ActivityParameterNode activityparameternode_src = null;
		ActivityParameterNode activityparameternode_dst = null;
		for(Element element : listOfElement)
		{
			if (element instanceof CallBehaviorAction)
			{
				CallBehaviorAction callbehaviour = (CallBehaviorAction) element;
				Activity activity = (Activity) callbehaviour.getBehavior();
				EAnnotation annotation = activity.getEAnnotation(CreatePapyrusBlock.UID_EANNOTATION);
				if (annotation != null)
				{
					EList<EObject> eobject_list = annotation.getContents();
					for (EObject eo : eobject_list)
					{
						if (eo instanceof Comment)
						{
							Comment c = (Comment) eo;
							String uid = c.getBody();
							if (uid.equals(block_source))
							{
								callbehaviour_src = callbehaviour;
							}
							if (uid.equals(block_target))
							{
								callbehaviour_dst = callbehaviour;
							}
						}
					}
				}
			}
			if (element instanceof ForkNode)
			{
				ForkNode forknode = (ForkNode) element;
				EAnnotation annotation = forknode.getEAnnotation(CreatePapyrusBlock.UID_EANNOTATION);
				if (annotation != null)
				{
					EList<EObject> eobject_list = annotation.getContents();
					for (EObject eo : eobject_list)
					{
						if (eo instanceof Comment)
						{
							Comment c = (Comment) eo;
							String uid = c.getBody();
							if (uid.equals(block_source))
							{
								forknode_src = forknode;
							}
							if (uid.equals(block_target))
							{
								forknode_dst = forknode;
							}
						}
					}
				}
			}
			if (element instanceof ActivityParameterNode)
			{
				ActivityParameterNode activityparameternode = (ActivityParameterNode) element;
				EAnnotation annotation = activityparameternode.getEAnnotation(CreatePapyrusBlock.UID_EANNOTATION);
				if (annotation != null)
				{
					EList<EObject> eobject_list = annotation.getContents();
					for (EObject eo : eobject_list)
					{
						if (eo instanceof Comment)
						{
							Comment c = (Comment) eo;
							String uid = c.getBody();
							if (uid.equals(block_source))
							{
								activityparameternode_src = activityparameternode;
							}
							if (uid.equals(block_target))
							{
								activityparameternode_dst = activityparameternode;
							}
						}
					}
				}
			}
		}
		
		// get source pin
		ActivityNode src_pin = null;
		EList<Element> listOfElementFromNamedElement = null;
		if (callbehaviour_src != null)
		{
			listOfElementFromNamedElement = callbehaviour_src.getOwnedElements();
			for (Element element : listOfElementFromNamedElement)
			{
				if (element instanceof ActivityNode)
				{
					ActivityNode pout = (ActivityNode) element;
					if (pout.getName().equals(port_src_name))
					{
						src_pin = pout;
					}
				}
			}
		} 
		else if (forknode_src != null)
		{
			src_pin = forknode_src;
		} else if (activityparameternode_src != null)
		{
			src_pin = activityparameternode_src;
		}
		
		// get target pin
		ActivityNode dst_pin = null;
		if (callbehaviour_dst != null)
		{
			listOfElementFromNamedElement = callbehaviour_dst.getOwnedElements();
			for (Element element : listOfElementFromNamedElement)
			{
				if (element instanceof ActivityNode)
				{
					ActivityNode pin = (ActivityNode) element;
					if (pin.getName().equals(port_dst_name))
					{
						dst_pin = pin;
					}
				}
			}
		} else if (forknode_dst != null)
		{
			dst_pin = forknode_dst;
		} else if (activityparameternode_dst != null)
		{
			dst_pin = activityparameternode_dst;
		}
		
		// create Object flow
		ObjectFlow objectFlow = (ObjectFlow) PapyrusLibrary.createSysmlElementFromUmlType(container, UMLElementTypes.OBJECT_FLOW, src_pin, dst_pin);
		PapyrusLibrary.modifyNameFromNamedElement(objectFlow, name);
		
		//addStereotypeFromObjectFlow(objectFlow, name);

		// get the unique diagram contained in this container (same name as the container)
		Diagram diagram = null;
		Object [] diagram_array = PapyrusLibrary.evaluate(container).toArray();
		if (diagram_array.length == 1)
		{
			diagram = (Diagram) diagram_array[0];
		}
		
		PapyrusLibrary.openDiagram(diagram);
		IEditorPart activeEditor = PapyrusLibrary.getISashWindowsContainer().getActiveEditor();
		EditPart editpart = (EditPart) activeEditor.getAdapter(EditPart.class);
		
		// pushing an SysML object to the current diagram graphically
		PapyrusLibrary.showElementIn(objectFlow, (DiagramEditor) activeEditor, editpart, 100, 100);
		log.info("<-- CreatePapyrusLink\n");
	}
	
	/**
	 * Public functions
	 */
	
	/**
	 * The main action of this class
	 * 
	 * @param parameters : the parameters to deal with
	 * @return a null object
	 */
	public static ArrayList<Object> executeTask(ArrayList<Object> parameters)
	{
		final String name = (String) parameters.get(0);
		final String block_source = (String) parameters.get(1);
		final String port_src_name = (String) parameters.get(2);
		final String block_target = (String) parameters.get(3);
		final String port_dst_name = (String) parameters.get(4);
		final String diagram_uid = (String) parameters.get(5);
		
		Display.getDefault().syncExec(
				new Runnable() 
				{
					public void run()
					{ 
						createSysmlItem(name, block_source, port_src_name, block_target, port_dst_name, diagram_uid);
					} 
				});
		
		return null;
	}
}
